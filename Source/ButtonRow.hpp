//
//  ButtonRow.hpp
//  JuceBasicWindow
//
//  Created by Callum McNeill on 13/11/2017.
//
//

#ifndef ButtonRow_hpp
#define ButtonRow_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "MyButton.hpp"

class ButtonRow : public Component
{
public:
    ButtonRow();
    ~ButtonRow();
    
    void resized() override;
    
    
private:
    MyButton button1;
    MyButton button2;
    MyButton button3;
    MyButton button4;
    MyButton button5;
    
};

#endif /* ButtonRow_hpp */

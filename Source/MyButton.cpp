//
//  MyButton.cpp
//  JuceBasicWindow
//
//  Created by Callum McNeill on 13/11/2017.
//
//

#include "MyButton.hpp"
#include "../JuceLibraryCode/JuceHeader.h"

//==============================================================================
MyButton::MyButton()
{
    //initial colour
    colour = Colours::green;
}

MyButton::~MyButton()
{
    
}

void MyButton::paint (Graphics& g)
{
    g.setColour(colour);
    
    g.fillRoundedRectangle(0, 0, getWidth(), getHeight(), 10);
}

void MyButton::mouseEnter (const MouseEvent& event)
{
    DBG("mouse enter");
    
    colour = Colours::forestgreen;
    
    repaint();
}

void MyButton::mouseExit (const MouseEvent& event)
{
    DBG("mouse exit");
    
    colour = Colours::green;
    
    repaint();
}

void MyButton::mouseUp (const MouseEvent& event)
{
    DBG("mouse up");
    
    colour = Colours::forestgreen;
    
    repaint();
}

void MyButton::mouseDown (const MouseEvent& event)
{
    DBG("mouse down");
    
    colour = Colours::darkgreen;
    
    repaint();
}


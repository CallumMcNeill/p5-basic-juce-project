//
//  MyButton.hpp
//  JuceBasicWindow
//
//  Created by Callum McNeill on 13/11/2017.
//
//

#ifndef MyButton_hpp
#define MyButton_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

class MyButton : public Component
{
public:
    //==============================================================================
    MyButton();
    ~MyButton();
    
    void paint (Graphics& g) override;
    void mouseEnter (const MouseEvent& event) override;
    void mouseExit (const MouseEvent& event) override;
    void mouseUp (const MouseEvent& event) override;
    void mouseDown (const MouseEvent& event) override;
    
private:
    Colour colour;
    Colour newColour;
};

    //==============================================================================
#endif /* MyButton_hpp */

/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    
    //add colour select
    //addAndMakeVisible (colourSelect1);
    
    //add my button
    //addAndMakeVisible (callumButton1);
    
    //add button row
    addAndMakeVisible(buttonRow1);
}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    DBG ("Print any old random text");
    DBG ("Print this variable: " << getWidth() << "and this variable: " << getHeight());
    
    //colourSelect1.setBounds (10, 10, getWidth() - 20, getHeight()/2); //colour select size
    
    //callumButton1.setBounds (10, 10, getWidth()/2, getHeight()/2); //my button set bounds
    
    buttonRow1.setBounds (0, 0, getWidth(), getHeight());
}

void MainComponent::mouseUp (const MouseEvent& event)
{
    DBG("mouse released");
    
    x = event.x;
    y = event.y;
    DBG("X: " << event.x << " Y: " << event.y);
    
    //repaint();
}
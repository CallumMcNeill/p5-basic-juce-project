//
//  ButtonRow.cpp
//  JuceBasicWindow
//
//  Created by Callum McNeill on 13/11/2017.
//
//

#include "ButtonRow.hpp"
#include "../JuceLibraryCode/JuceHeader.h"

ButtonRow::ButtonRow()
{
    addAndMakeVisible(button1);
    addAndMakeVisible(button2);
    addAndMakeVisible(button3);
    addAndMakeVisible(button4);
    addAndMakeVisible(button5);
}

ButtonRow::~ButtonRow()
{
    
}

void ButtonRow::resized()
{
    button1.setBounds (0, 10, (getWidth()/5), getHeight());
    button2.setBounds (getWidth()/5, 10, (getWidth()/5), getHeight());
    button3.setBounds ((getWidth()/5)*2, 10, (getWidth()/5), getHeight());
    button4.setBounds ((getWidth()/5)*3, 10, (getWidth()/5), getHeight());
    button5.setBounds ((getWidth()/5)*4, 10, (getWidth()/5), getHeight());
}
